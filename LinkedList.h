#ifndef LINKEDLIST
#define LINKEDLIST
#include<iostream>
template<typename T>
class LinkedList {

 public:
  struct Node {
    T data;
    Node* next;
    Node* prev;
    Node(){
      next=this;
      prev=this;
    }
  Node(const T& inp):data(inp),next(this),prev(this){};
  Node(const T& inp, Node* inext, Node* iprev):data(inp),next(inext),prev(iprev){};
    void insert(T& a) {data = a;};
    void insertPrev(T& a) {prev = a;};
    void insertNext(T& a) {next = a;};
    T getData(){return *data;};
    Node* getNext(){return next;};
    Node* getPrev(){return prev;};
  };
  Node sentinel;
  int len;
  typedef T value_type;
  class iterator {
    Node* p;
  public:
  iterator(Node* l):p{l}{};
    T &operator*() {
      return p->data;
    }
    bool operator==(const iterator &i) const {
      return p==i.p;
    }
    bool operator!=(const iterator &i) const {
      return !(p==i.p);
    }
    iterator &operator=(const iterator &i) {
      p=i.p;
      return *this;
    }
    iterator &operator++() {
      p=p->next;
      return *this;
    }
    iterator &operator--() {
      p=p->prev;
      return *this;
    }
    iterator operator++(int) {
      iterator ret(*this);
      p=p->next;
      return ret;
    }
    iterator operator--(int) {
      iterator ret(*this);
      p = p->prev;
      return ret;
    }
    friend class const_iterator;
    friend class LinkedList;
  };
  class const_iterator {
    const Node* p;
  public:
  const_iterator(const Node* l):p{l}{};
    const T &operator*() {
      return p->data;
    }
    bool operator==(const const_iterator &i) const {
      return p == i.p;
    }
    bool operator!=(const const_iterator &i) const {
      return !(p==i.p);
    }
    const_iterator &operator=(const const_iterator &i) {
      p=i.p;
      return *this;
    }
    const_iterator &operator++() {
      p=p->next;
      return *this;
    }
    const_iterator &operator--() {
      p=p->prev;
      return *this;
    }
    const_iterator operator++(int) {
      const_iterator ret(*this);
      p=p->next;
      return ret;
    }
    const_iterator operator--(int) {
      const_iterator ret(*this);
      p=p->prev;
      return ret;
    }
  };
 LinkedList():sentinel(), len{0}{};
  LinkedList(const LinkedList& that); 
  LinkedList &operator=(const LinkedList &al) {
    clear();
    for(const T& d:al) {
      push_back(d);
    }
    len = al.len;
    return *this;
  }
  ~LinkedList(){clear();};
  void push_back(const T &t);        
  void pop_back();             
  int size() const {return len;}; 
  void clear() {
    for(Node *rover=sentinel.next,*n; rover!=&sentinel; n=rover->next,delete rover,rover=n);
    sentinel.next=&sentinel;
    sentinel.prev=&sentinel;
    len = 0;
  }; 
  iterator insert(iterator position,const T &t){ 
    Node* iprev = position.p -> prev;
    Node* inext = position.p->next;
    Node* tmp = new Node();
    position.p->prev->next = tmp;
    tmp->data = t;
    tmp->next = position.p;
    tmp->prev = iprev;
    position.p->prev = tmp;
    len++;
    return --position;
  }
  void verifyList() {
      int sizetmp = 0;
      for(Node* i = sentinel.next;i!=&sentinel;i=i->next){
        sizetmp++;
        if((i->prev == nullptr) || (i->next == nullptr)){
          std::cout << "nullptr at: " << i->data << "\n";
        }
        if((i->prev->next != i) || (i->next->prev != i)){
          std::cout << "rover next or prev not equal to rover\n";
        }
      }
      if(len != sizetmp){
        std::cout << "different size\n";
      }
}
  const T &operator[](int index) const;      
  T &operator[](int index); 
  iterator erase(iterator position){
    Node* tmp2 = position.p->next;
    position.p->prev->next = position.p->next;
    position.p->next->prev = position.p->prev;
    delete position.p;
    len--;
    return tmp2;
  }
  iterator begin(){
    return iterator(sentinel.next);
  };
  const_iterator begin() const {
    return const_iterator(sentinel.next);
  };
  iterator end(){
    return iterator(&sentinel);
  };
  const_iterator end() const {
    return const_iterator(&sentinel);
  };
  const_iterator cbegin() const {
    return const_iterator(sentinel.next);
  };
  const_iterator cend() const {
    return const_iterator(&sentinel);
  };
};
template<typename T>
const T& LinkedList<T>::operator[](int index) const {
  const Node* rover = sentinel.next;
  for(int i=0;i<index;i++){
    rover = rover->next;
  }
  return rover.getData();
}
template<typename T>
T& LinkedList<T>::operator[](int index) {
  Node* rover = sentinel.next;
  for(int i = 0; i<index;i++){
    rover = rover->next;
  }
  return rover->data;
}
template<typename T>
LinkedList<T>::LinkedList(const LinkedList<T>& that) {
  for(const T& d:that) {
    push_back(d);
  }
  len = that.len;
}
template<typename T>
void LinkedList<T>::pop_back(){
  Node* tmp = sentinel.prev;
  delete sentinel.prev;
 sentinel.prev = tmp -> prev;
  sentinel.prev ->next = &sentinel;
  len--;
}
template<typename T>
void LinkedList<T>::push_back(const T &t){
  Node* tmp = new Node(t,&sentinel,(sentinel.prev));
  sentinel.prev->next = tmp;
  sentinel.prev = tmp;
  len++;
}
#endif
